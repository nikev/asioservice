#include "AsioService.h"
#include <time.h>

AsioConnection::AsioConnection(asio::io_service& service, unsigned bufferSize, onAsioConnectCallback connectCallback, onAsioReadCallback readCallback, onAsioWriteCallback writeCallback, void* userData): 
	_socket(service), _connected(false), _reading(false), _writing(false), _readTimer(0), _writeTimer(0), _onConnectCallback(connectCallback), _onReadCallback(readCallback), _onWriteCallback(writeCallback), _userData(userData)
{
	_readData.resize(bufferSize, 0);
}

AsioConnection::~AsioConnection() {}

bool AsioConnection::beginRead()
{
	if (_connected)
	{
		unsigned currentTime = time(0);

		if (_reading && currentTime - _readTimer > 10)
		{
			disconnect();
			return false;
		}

		if (!_reading)
		{
			try
			{
				_reading = true;
				_readTimer = currentTime;
				asio::async_read(_socket, asio::buffer(_readData), asio::transfer_at_least(1), std::bind(&AsioConnection::_onRead, shared_from_this(), std::placeholders::_1, std::placeholders::_2));
				return true;
			}
			catch (...)
			{
				disconnect();
			}
		}
	}

	return false;
}

bool AsioConnection::beginWrite(const std::string& message)
{
	if (_connected)
	{
		unsigned currentTime = time(0);

		if (_writing && currentTime - _writeTimer > 10)
		{
			disconnect();
			return false;
		}

		if (!_writing)
		{
			try
			{
				_writing = true;
				_writeTimer = currentTime;
				_writeData.resize(message.size());
				memcpy(&_writeData[0], message.c_str(), message.size());
				asio::async_write(_socket, asio::buffer(_writeData), std::bind(&AsioConnection::_onWrite, shared_from_this(), std::placeholders::_1, std::placeholders::_2));
				return true;
			}
			catch (...)
			{
				disconnect();
			}
		}
	}

	return false;
}

void AsioConnection::_onRead(const asio::error_code& error, size_t bytes_transferred)
{
	_reading = false;
	if (!error)
	{
		if (bytes_transferred != 0)
		{
			std::string str(&_readData[0], bytes_transferred);
			_onReadCallback(shared_from_this(), str);
		}
	}
	else
	{
		disconnect();
		std::string str;
		_onReadCallback(shared_from_this(), str);
	}
}

void AsioConnection::_onWrite(const asio::error_code& error, size_t bytes_transferred)
{
	if (error.value() != 0)
		disconnect();

	_writing = false;
	_onWriteCallback(shared_from_this());
}

bool AsioConnection::isConnected()
{
	return _connected;
}

void AsioConnection::disconnect()
{
	bool wasConnected = _connected;

	try
	{
		_reading = false;
		_writing = false;
		_readTimer = 0;
		_writeTimer = 0;
		_connected = false;

		if (_socket.is_open())
		{
			_socket.close();
		}
	}
	catch(...) {}

	if(wasConnected)
		_onConnectCallback(shared_from_this());
}

void* AsioConnection::getUserData()
{
	return _userData;
}

void AsioConnection::setUserData(void* userData)
{
	_userData = userData;
}

void AsioConnection::setOnConnectCallback(onAsioConnectCallback onConnectCallback)
{
    _onConnectCallback = onConnectCallback;
}

void AsioConnection::setOnReadCallback(onAsioReadCallback onReadCallback)
{
    _onReadCallback = onReadCallback;
}

void AsioConnection::setOnWriteCallback(onAsioWriteCallback onWriteCallback)
{
    _onWriteCallback = onWriteCallback;
}

AsioService::AsioService(): _service(), _work(_service) {}

AsioService::~AsioService()
{
	if (_acceptor)
	{
		try
		{
			_acceptor->close();
		}
		catch(...){}
	}
}

AsioService::AsioService(onAsioConnectCallback connectCallback, onAsioReadCallback readCallback, onAsioWriteCallback writeCallback, unsigned bufferSize):
	_onConnectCallback(connectCallback), _onReadCallback(readCallback), _onWriteCallback(writeCallback), _service(), _work(_service), _acceptor(0), _connection(0), _bufferSize(bufferSize), _userData(0)  {}

bool AsioService::isConnecting()
{
	if (_connection)
		return true;

	return false;
}

void AsioService::update()
{
	_service.poll();
}

bool AsioService::host(unsigned short port)
{
	if (!_acceptor)
	{
		try
		{
			_acceptor = std::make_shared<asio::ip::tcp::acceptor>(_service, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port));
			return _beginAccept();
		}
		catch (...)
		{
			_acceptor = 0;
		}
	}
	return false;
}

void AsioService::setUserData(void* userdata)
{
	_userData = userdata;
}

void* AsioService::getUserData()
{
	return _userData;
}

bool AsioService::_beginAccept()
{
	try
	{
		std::shared_ptr<AsioConnection> connection = std::make_shared<AsioConnection>(_acceptor->get_io_service(), _bufferSize, _onConnectCallback, _onReadCallback, _onWriteCallback, _userData);
		_acceptor->async_accept(connection->_socket, std::bind(&AsioService::_onAccept, this, connection, std::placeholders::_1));
		return true;
	}
	catch(...) {}

	return false;
}

void AsioService::_onAccept(std::shared_ptr<AsioConnection> connection, const asio::error_code& error)
{
	if (!error)
	{
		connection->_connected = true;
		_onConnectCallback(connection);
	}

	_beginAccept();
}

bool AsioService::beginConnect(const std::string& address, int port)
{
	if (!_connection)
	{
		try
		{
			std::stringstream portstr;
			portstr << port;
			asio::ip::tcp::resolver resolver(_service);
			asio::ip::tcp::resolver::iterator endpoint = resolver.resolve(asio::ip::tcp::resolver::query(address.c_str(), portstr.str()));
			_connection = std::make_shared<AsioConnection>(_service, _bufferSize, _onConnectCallback, _onReadCallback, _onWriteCallback, _userData);
			asio::async_connect(_connection->_socket.lowest_layer(), endpoint, std::bind(&AsioService::_onConnect, this, std::placeholders::_1));
			return true;
		}
		catch (...) {}
		_connection = 0;
	}

	return false;
}

void AsioService::_onConnect(const std::error_code& error)
{
	_connection->_connected = error.value() == 0;
	_onConnectCallback(_connection);
	_connection = 0;
}
