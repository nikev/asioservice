# AsioService

A wrapper to simplify the use of [Asio](https://think-async.com/Asio/).

# Installation

- AsioService uses the [Asio variant](https://think-async.com/Asio/AsioAndBoostAsio.html) that is header-only and does not use Boost.
- Download Asio zip file and extract it.
- Add the `include` folder your project's includes.
- Add AsioService.cpp and AsioService.h to your project.

# Usage

```
void onConnect(std::shared_ptr<AsioConnection> connection){
    if(connection->isConnected()){
        std::cout << "Connected!" << std::endl;
        connection->beginRead();
    } else {
        std::cout << "Disconnected!" << std::endl;
    }
}

void onRead(std::shared_ptr<AsioConnection> connection, const std::string& data){
    std::cout << "Received: " << data << std::endl;
    std::string writeData = "hello world!";
    connection->beginWrite(writeData);
}

void onWrite(std::shared_ptr<AsioConnection> connection){
    std::cout << "Wrote data!" << std::endl;
    if(connection->isConnectioned()){
        connection->disconnect();
    }
}

AsioService asioService(onConnect, onRead, onWrite, 1000000);//1MB buffer

if(asioService.host(8080)){
    std::cout << "Hosting!" << std::endl;
}

while(true){
    asioService.update();
}
```

See [AsioService.h](https://gitlab.com/nikev/asioservice/blob/master/AsioService.h) for more functionality.
