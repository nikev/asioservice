#ifndef __ASYNCSOCKET_H__
#define __ASYNCSOCKET_H__

#include <asio.hpp>
#include <asio/ip/tcp.hpp>
#include <string>
#include <functional>
#include <memory>
#include <vector>

class AsioConnection;
class AsioService;

typedef void(*onAsioConnectCallback)(std::shared_ptr<AsioConnection> connection);
typedef void(*onAsioReadCallback)(std::shared_ptr<AsioConnection> connection, const std::string& data);
typedef void(*onAsioWriteCallback)(std::shared_ptr<AsioConnection> connection);

class AsioConnection : public std::enable_shared_from_this<AsioConnection>
{
    friend class AsioService;

    asio::ip::tcp::socket _socket;
    std::vector<char>     _readData;
    std::vector<char>     _writeData;
    bool                  _connected;
    bool                  _reading;
    bool                  _writing;
    unsigned              _readTimer;;
    unsigned              _writeTimer;
    onAsioConnectCallback _onConnectCallback;
    onAsioReadCallback    _onReadCallback;
    onAsioWriteCallback   _onWriteCallback;
    void*                 _userData;

    void _onRead(const asio::error_code& error, size_t bytes_transferred);
    void _onWrite(const asio::error_code& error, size_t bytes_transferred);

public:
    AsioConnection(asio::io_service& service, unsigned bufferSize, onAsioConnectCallback connectCallback, onAsioReadCallback readCallback, onAsioWriteCallback writeCallback, void* userData);
    virtual ~AsioConnection();

    void disconnect();
    bool isConnected();
    bool beginRead();
    bool beginWrite(const std::string& message);
    void* getUserData();
    void setUserData(void* userData);

    void setOnConnectCallback(onAsioConnectCallback onConnectCallback);
    void setOnReadCallback(onAsioReadCallback onReadCallback);
    void setOnWriteCallback(onAsioWriteCallback onWriteCallback);
};

class AsioService
{
    onAsioConnectCallback                    _onConnectCallback;
    onAsioReadCallback                       _onReadCallback;
    onAsioWriteCallback                      _onWriteCallback;
    asio::io_service                         _service;
    asio::io_service::work                   _work;
    std::shared_ptr<asio::ip::tcp::acceptor> _acceptor;
    std::shared_ptr<AsioConnection>          _connection;
    unsigned                                 _bufferSize;
    void*                                    _userData;

    bool _beginAccept();
    void _onAccept(std::shared_ptr<AsioConnection> connection, const asio::error_code& error);
    void _onConnect(const std::error_code& error);

    AsioService();

public:
    virtual ~AsioService();
    AsioService(onAsioConnectCallback connectCallback, onAsioReadCallback readCallback, onAsioWriteCallback writeCallback, unsigned bufferSize);

    bool beginConnect(const std::string& address, int port);
    bool isConnecting();
    bool host(unsigned short port);
    void update();

    void setUserData(void* userdata);
    void* getUserData();
};

#endif
